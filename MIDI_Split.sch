EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R1
U 1 1 5BD09D28
P 1800 1000
F 0 "R1" V 1880 1000 50  0000 C CNN
F 1 "220R" V 1800 1000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 1730 1000 50  0001 C CNN
F 3 "" H 1800 1000 50  0001 C CNN
	1    1800 1000
	0    1    1    0   
$EndComp
$Comp
L MIDI_Split-rescue:D-MIDI_Split-rescue-MIDI_Split-rescue D1
U 1 1 5BD09D6D
P 2100 1350
F 0 "D1" H 2100 1450 50  0000 C CNN
F 1 "1N4148" H 2100 1250 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P7.62mm_Horizontal" H 2100 1350 50  0001 C CNN
F 3 "" H 2100 1350 50  0001 C CNN
	1    2100 1350
	0    1    1    0   
$EndComp
$Comp
L power:+9V #PWR015
U 1 1 5BD0AC63
P 6750 900
F 0 "#PWR015" H 6750 750 50  0001 C CNN
F 1 "+9V" H 6750 1040 50  0000 C CNN
F 2 "MIDI_Split:battery" H 6750 900 50  0001 C CNN
F 3 "" H 6750 900 50  0001 C CNN
	1    6750 900 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR018
U 1 1 5BD0AC9A
P 9100 900
F 0 "#PWR018" H 9100 750 50  0001 C CNN
F 1 "+5V" H 9100 1040 50  0000 C CNN
F 2 "" H 9100 900 50  0001 C CNN
F 3 "" H 9100 900 50  0001 C CNN
	1    9100 900 
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:L7805-MIDI_Split-rescue-MIDI_Split-rescue U3
U 1 1 5BD0ACD1
P 7950 1100
F 0 "U3" H 7800 1225 50  0000 C CNN
F 1 "L7805" H 7950 1225 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7975 950 50  0001 L CIN
F 3 "" H 7950 1050 50  0001 C CNN
	1    7950 1100
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:CP-MIDI_Split-rescue-MIDI_Split-rescue C1
U 1 1 5BD0BD43
P 7050 1400
F 0 "C1" H 7075 1500 50  0000 L CNN
F 1 "10u" H 7075 1300 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 7088 1250 50  0001 C CNN
F 3 "" H 7050 1400 50  0001 C CNN
	1    7050 1400
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:CP-MIDI_Split-rescue-MIDI_Split-rescue C3
U 1 1 5BD0BD91
P 8850 1400
F 0 "C3" H 8875 1500 50  0000 L CNN
F 1 "10u" H 8875 1300 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 8888 1250 50  0001 C CNN
F 3 "" H 8850 1400 50  0001 C CNN
	1    8850 1400
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:C-MIDI_Split-rescue-MIDI_Split-rescue C2
U 1 1 5BD0BDE5
P 8450 1400
F 0 "C2" H 8475 1500 50  0000 L CNN
F 1 "100n" H 8475 1300 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 8488 1250 50  0001 C CNN
F 3 "" H 8450 1400 50  0001 C CNN
	1    8450 1400
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:2N3904-MIDI_Split-rescue-MIDI_Split-rescue Q1
U 1 1 5BD0C20D
P 4800 950
F 0 "Q1" H 5000 1025 50  0000 L CNN
F 1 "2N3904" H 5000 950 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline_Wide" H 5000 875 50  0001 L CIN
F 3 "" H 4800 950 50  0001 L CNN
	1    4800 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR06
U 1 1 5BD1B0EC
P 4900 750
F 0 "#PWR06" H 4900 600 50  0001 C CNN
F 1 "+5V" H 4900 890 50  0000 C CNN
F 2 "" H 4900 750 50  0001 C CNN
F 3 "" H 4900 750 50  0001 C CNN
	1    4900 750 
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R4
U 1 1 5BD1B327
P 4900 1300
F 0 "R4" V 4980 1300 50  0000 C CNN
F 1 "220R" V 4900 1300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 4830 1300 50  0001 C CNN
F 3 "" H 4900 1300 50  0001 C CNN
	1    4900 1300
	-1   0    0    1   
$EndComp
$Comp
L MIDI_Split-rescue:LED-MIDI_Split-rescue-MIDI_Split-rescue D2
U 1 1 5BD1C199
P 4900 1700
F 0 "D2" H 4900 1800 50  0000 C CNN
F 1 "LED" H 4900 1600 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 4900 1700 50  0001 C CNN
F 3 "" H 4900 1700 50  0001 C CNN
	1    4900 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 1000 2100 1000
Wire Wire Line
	2100 1000 2100 1200
Wire Wire Line
	2100 1500 2100 1700
Wire Wire Line
	1300 1700 1750 1700
Connection ~ 2100 1000
Wire Wire Line
	2600 1200 2450 1200
Wire Wire Line
	6750 1850 6750 1100
Wire Wire Line
	6750 1100 7050 1100
Wire Wire Line
	8250 1100 8450 1100
Wire Wire Line
	9100 1100 9100 900 
Wire Wire Line
	7050 1750 7050 1650
Connection ~ 7950 1650
Wire Wire Line
	8850 1650 8850 1550
Wire Wire Line
	8450 1550 8450 1650
Connection ~ 8450 1650
Wire Wire Line
	7050 1100 7050 1250
Connection ~ 7050 1100
Wire Wire Line
	8450 1100 8450 1250
Connection ~ 8450 1100
Wire Wire Line
	8850 1250 8850 1100
Connection ~ 8850 1100
Wire Wire Line
	4900 1450 4900 1550
$Comp
L power:VCC #PWR017
U 1 1 5BD0C951
P 8650 1000
F 0 "#PWR017" H 8650 850 50  0001 C CNN
F 1 "VCC" H 8650 1150 50  0000 C CNN
F 2 "" H 8650 1000 50  0001 C CNN
F 3 "" H 8650 1000 50  0001 C CNN
	1    8650 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 1000 8650 1100
Connection ~ 8650 1100
$Comp
L power:GND #PWR016
U 1 1 5BD7623B
P 7950 1750
F 0 "#PWR016" H 7950 1500 50  0001 C CNN
F 1 "GND" H 7950 1600 50  0000 C CNN
F 2 "" H 7950 1750 50  0001 C CNN
F 3 "" H 7950 1750 50  0001 C CNN
	1    7950 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1750 7950 1650
Connection ~ 6750 1100
Connection ~ 7050 1650
Wire Wire Line
	6400 1750 6550 1750
$Comp
L MIDI_Split-rescue:Conn_01x02-MIDI_Split-rescue-MIDI_Split-rescue J5
U 1 1 5BD77977
P 6200 1850
F 0 "J5" H 6200 1950 50  0000 C CNN
F 1 "Battery" H 6200 1650 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 6200 1850 50  0001 C CNN
F 3 "" H 6200 1850 50  0001 C CNN
	1    6200 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 1700 2200 1700
Wire Wire Line
	7950 1650 7950 1400
Wire Wire Line
	7950 1650 8450 1650
Wire Wire Line
	8450 1650 8850 1650
Wire Wire Line
	7050 1100 7650 1100
Wire Wire Line
	8450 1100 8650 1100
Wire Wire Line
	8850 1100 9100 1100
Wire Wire Line
	8650 1100 8850 1100
Wire Wire Line
	6750 1100 6750 900 
Wire Wire Line
	7050 1650 7050 1550
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R3
U 1 1 5BD19B56
P 4450 950
F 0 "R3" V 4530 950 50  0000 C CNN
F 1 "10K" V 4450 950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 4380 950 50  0001 C CNN
F 3 "" H 4450 950 50  0001 C CNN
	1    4450 950 
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS14 U2
U 1 1 5BD0AB4A
P 4600 2600
F 0 "U2" H 4750 2700 50  0000 C CNN
F 1 "74LS14" H 4800 2500 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4600 2600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 4600 2600 50  0001 C CNN
	1    4600 2600
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R6
U 1 1 5BD0CC4E
P 5950 4150
F 0 "R6" V 6030 4150 50  0000 C CNN
F 1 "220R" V 5950 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 5880 4150 50  0001 C CNN
F 3 "" H 5950 4150 50  0001 C CNN
	1    5950 4150
	0    -1   -1   0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R7
U 1 1 5BD0CCAF
P 5950 4800
F 0 "R7" V 6030 4800 50  0000 C CNN
F 1 "220R" V 5950 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 5880 4800 50  0001 C CNN
F 3 "" H 5950 4800 50  0001 C CNN
	1    5950 4800
	0    -1   -1   0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R8
U 1 1 5BD0CD17
P 5950 5450
F 0 "R8" V 6030 5450 50  0000 C CNN
F 1 "220R" V 5950 5450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 5880 5450 50  0001 C CNN
F 3 "" H 5950 5450 50  0001 C CNN
	1    5950 5450
	0    -1   -1   0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R5
U 1 1 5BD103B0
P 5950 3500
F 0 "R5" V 6030 3500 50  0000 C CNN
F 1 "220R" V 5950 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 5880 3500 50  0001 C CNN
F 3 "" H 5950 3500 50  0001 C CNN
	1    5950 3500
	0    1    1    0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R9
U 1 1 5BD10B5D
P 5950 6100
F 0 "R9" V 6030 6100 50  0000 C CNN
F 1 "220R" V 5950 6100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 5880 6100 50  0001 C CNN
F 3 "" H 5950 6100 50  0001 C CNN
	1    5950 6100
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74LS14 U2
U 7 1 603223E2
P 9950 2550
F 0 "U2" H 10100 2650 50  0000 C CNN
F 1 "74LS14" H 10150 2450 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 9950 2550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 9950 2550 50  0001 C CNN
	7    9950 2550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR019
U 1 1 603363D5
P 9450 2550
F 0 "#PWR019" H 9450 2300 50  0001 C CNN
F 1 "GND" H 9455 2377 50  0000 C CNN
F 2 "" H 9450 2550 50  0001 C CNN
F 3 "" H 9450 2550 50  0001 C CNN
	1    9450 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR020
U 1 1 603372A7
P 10450 2550
F 0 "#PWR020" H 10450 2400 50  0001 C CNN
F 1 "+5V" H 10465 2723 50  0000 C CNN
F 2 "" H 10450 2550 50  0001 C CNN
F 3 "" H 10450 2550 50  0001 C CNN
	1    10450 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR08
U 1 1 604482EE
P 5800 3500
F 0 "#PWR08" H 5800 3350 50  0001 C CNN
F 1 "+5V" H 5815 3673 50  0000 C CNN
F 2 "" H 5800 3500 50  0001 C CNN
F 3 "" H 5800 3500 50  0001 C CNN
	1    5800 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS14 U2
U 5 1 5BD0B4BE
P 7300 5450
F 0 "U2" H 7450 5550 50  0000 C CNN
F 1 "74LS14" H 7500 5350 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7300 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 7300 5450 50  0001 C CNN
	5    7300 5450
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LS14 U2
U 4 1 5BD0B470
P 7300 4800
F 0 "U2" H 7450 4900 50  0000 C CNN
F 1 "74LS14" H 7500 4700 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7300 4800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 7300 4800 50  0001 C CNN
	4    7300 4800
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LS14 U2
U 3 1 5BD0B431
P 7300 4150
F 0 "U2" H 7450 4250 50  0000 C CNN
F 1 "74LS14" H 7500 4050 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7300 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 7300 4150 50  0001 C CNN
	3    7300 4150
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LS14 U2
U 2 1 5BD0B3EB
P 7300 3500
F 0 "U2" H 7450 3600 50  0000 C CNN
F 1 "74LS14" H 7500 3400 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7300 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 7300 3500 50  0001 C CNN
	2    7300 3500
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LS14 U2
U 6 1 5BD0B724
P 7300 6100
F 0 "U2" H 7450 6200 50  0000 C CNN
F 1 "74LS14" H 7500 6000 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 7300 6100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS14" H 7300 6100 50  0001 C CNN
	6    7300 6100
	-1   0    0    1   
$EndComp
NoConn ~ 6100 3600
NoConn ~ 6100 4250
NoConn ~ 6100 4900
NoConn ~ 6100 5550
Wire Wire Line
	4900 2600 7600 2600
Wire Wire Line
	7600 2600 7600 3500
Connection ~ 7600 3500
Wire Wire Line
	7600 3500 7600 4150
Connection ~ 7600 4150
Wire Wire Line
	7600 4150 7600 4800
Connection ~ 7600 4800
Wire Wire Line
	7600 4800 7600 5450
Connection ~ 7600 5450
Wire Wire Line
	7600 5450 7600 6100
Wire Wire Line
	5800 3500 5800 4150
Connection ~ 5800 3500
Connection ~ 5800 4150
Wire Wire Line
	5800 4150 5800 4800
Connection ~ 5800 4800
Wire Wire Line
	5800 4800 5800 5450
Connection ~ 5800 5450
Wire Wire Line
	5800 5450 5800 6100
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 601EBA15
P 1550 1950
F 0 "J4" V 1514 1762 50  0000 R CNN
F 1 "Conn_01x03" V 1423 1762 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1550 1950 50  0001 C CNN
F 3 "~" H 1550 1950 50  0001 C CNN
	1    1550 1950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 2150 1750 2150
Wire Wire Line
	1750 2150 1750 1700
Wire Wire Line
	1550 2150 1550 2350
Wire Wire Line
	1550 2350 2200 2350
Wire Wire Line
	2200 2350 2200 1700
Connection ~ 2200 1700
Wire Wire Line
	2200 1700 2450 1700
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 601FA466
P 1000 750
F 0 "J2" V 964 562 50  0000 R CNN
F 1 "Conn_01x03" V 873 562 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1000 750 50  0001 C CNN
F 3 "~" H 1000 750 50  0001 C CNN
	1    1000 750 
	0    -1   -1   0   
$EndComp
$Comp
L MIDI_Split-rescue:DIN-5_180degree-MIDI_Split-rescue-MIDI_Split-rescue J3
U 1 1 5BD09CF0
P 1400 1350
F 0 "J3" H 1525 1575 50  0000 C CNN
F 1 "MIDI_In" H 1400 1100 50  0000 C CNN
F 2 "MIDI_DIN:DIN_5_180_Degrees" H 1400 1350 50  0001 C CNN
F 3 "" H 1400 1350 50  0001 C CNN
	1    1400 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1000 950  1000 1000
Wire Wire Line
	1000 1000 1650 1000
Wire Wire Line
	900  950  750  950 
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 601C6CF3
P 950 2600
F 0 "J1" H 1030 2592 50  0000 L CNN
F 1 "Conn_01x04" H 1030 2501 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 950 2600 50  0001 C CNN
F 3 "~" H 950 2600 50  0001 C CNN
	1    950  2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2600 750  2600
Wire Wire Line
	1450 2150 1450 2600
Wire Wire Line
	750  950  750  2500
$Comp
L power:+9V #PWR01
U 1 1 601EB4B7
P 750 2700
F 0 "#PWR01" H 750 2550 50  0001 C CNN
F 1 "+9V" V 765 2828 50  0000 L CNN
F 2 "" H 750 2700 50  0001 C CNN
F 3 "" H 750 2700 50  0001 C CNN
	1    750  2700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 601EBF91
P 750 2800
F 0 "#PWR02" H 750 2550 50  0001 C CNN
F 1 "GND" V 755 2672 50  0000 R CNN
F 2 "" H 750 2800 50  0001 C CNN
F 3 "" H 750 2800 50  0001 C CNN
	1    750  2800
	0    1    1    0   
$EndComp
$Comp
L power:+9V #PWR09
U 1 1 601F24D9
P 6100 7300
F 0 "#PWR09" H 6100 7150 50  0001 C CNN
F 1 "+9V" V 6115 7428 50  0000 L CNN
F 2 "" H 6100 7300 50  0001 C CNN
F 3 "" H 6100 7300 50  0001 C CNN
	1    6100 7300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 601F24DF
P 6100 7400
F 0 "#PWR010" H 6100 7150 50  0001 C CNN
F 1 "GND" V 6105 7272 50  0000 R CNN
F 2 "" H 6100 7400 50  0001 C CNN
F 3 "" H 6100 7400 50  0001 C CNN
	1    6100 7400
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 2600 4300 2600
$Comp
L power:GND #PWR03
U 1 1 601E5D96
P 1100 1350
F 0 "#PWR03" H 1100 1100 50  0001 C CNN
F 1 "GND" V 1105 1222 50  0000 R CNN
F 2 "" H 1100 1350 50  0001 C CNN
F 3 "" H 1100 1350 50  0001 C CNN
	1    1100 1350
	0    1    1    0   
$EndComp
$Comp
L Isolator:6N136 U1
U 1 1 601E9453
P 2900 1100
F 0 "U1" H 2900 1525 50  0000 C CNN
F 1 "6N136" H 2900 1434 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2700 800 50  0001 L CIN
F 3 "https://optoelectronics.liteon.com/upload/download/DS70-2008-0032/6N135-L%206N136-L%20series.pdf" H 2900 1100 50  0001 L CNN
	1    2900 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 601F6EEB
P 3550 900
F 0 "R2" V 3343 900 50  0000 C CNN
F 1 "10K" V 3434 900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 3480 900 50  0001 C CNN
F 3 "~" H 3550 900 50  0001 C CNN
	1    3550 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 900  3300 900 
$Comp
L power:+5V #PWR05
U 1 1 601F97CF
P 3300 900
F 0 "#PWR05" H 3300 750 50  0001 C CNN
F 1 "+5V" H 3315 1073 50  0000 C CNN
F 2 "" H 3300 900 50  0001 C CNN
F 3 "" H 3300 900 50  0001 C CNN
	1    3300 900 
	1    0    0    -1  
$EndComp
Connection ~ 3300 900 
Wire Wire Line
	3300 900  3400 900 
$Comp
L power:GND #PWR04
U 1 1 601FA0C1
P 3200 1300
F 0 "#PWR04" H 3200 1050 50  0001 C CNN
F 1 "GND" H 3205 1127 50  0000 C CNN
F 2 "" H 3200 1300 50  0001 C CNN
F 3 "" H 3200 1300 50  0001 C CNN
	1    3200 1300
	1    0    0    -1  
$EndComp
NoConn ~ 3200 1000
Wire Wire Line
	4050 2600 4050 1200
Wire Wire Line
	4050 900  3700 900 
Wire Wire Line
	3200 1200 4050 1200
Connection ~ 4050 1200
Wire Wire Line
	4050 1200 4050 900 
Wire Wire Line
	7050 1650 7950 1650
$Comp
L power:PWR_FLAG #FLG01
U 1 1 602392F7
P 6550 1750
F 0 "#FLG01" H 6550 1825 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 1923 50  0000 C CNN
F 2 "" H 6550 1750 50  0001 C CNN
F 3 "~" H 6550 1750 50  0001 C CNN
	1    6550 1750
	1    0    0    -1  
$EndComp
Connection ~ 6550 1750
Wire Wire Line
	6550 1750 7050 1750
NoConn ~ 1400 1050
NoConn ~ 1400 1650
Connection ~ 6600 1850
Wire Wire Line
	6600 1850 6750 1850
Wire Wire Line
	6400 1850 6600 1850
$Comp
L power:PWR_FLAG #FLG02
U 1 1 60239FBC
P 6600 1850
F 0 "#FLG02" H 6600 1925 50  0001 C CNN
F 1 "PWR_FLAG" H 6600 2023 50  0000 C CNN
F 2 "" H 6600 1850 50  0001 C CNN
F 3 "~" H 6600 1850 50  0001 C CNN
	1    6600 1850
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 602652F9
P 4900 1850
F 0 "#PWR07" H 4900 1600 50  0001 C CNN
F 1 "GND" H 4905 1677 50  0000 C CNN
F 2 "" H 4900 1850 50  0001 C CNN
F 3 "" H 4900 1850 50  0001 C CNN
	1    4900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1000 2600 1000
Wire Wire Line
	2450 1200 2450 1700
Wire Wire Line
	4300 950  4300 2600
Connection ~ 4300 2600
NoConn ~ 5300 2050
$Comp
L power:GND #PWR014
U 1 1 601CEF9A
P 6400 5250
F 0 "#PWR014" H 6400 5000 50  0001 C CNN
F 1 "GND" V 6405 5077 50  0000 C CNN
F 2 "" H 6400 5250 50  0001 C CNN
F 3 "" H 6400 5250 50  0001 C CNN
	1    6400 5250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 601CCDDC
P 6400 4600
F 0 "#PWR013" H 6400 4350 50  0001 C CNN
F 1 "GND" V 6405 4427 50  0000 C CNN
F 2 "" H 6400 4600 50  0001 C CNN
F 3 "" H 6400 4600 50  0001 C CNN
	1    6400 4600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 601C0A98
P 6400 3950
F 0 "#PWR012" H 6400 3700 50  0001 C CNN
F 1 "GND" V 6405 3777 50  0000 C CNN
F 2 "" H 6400 3950 50  0001 C CNN
F 3 "" H 6400 3950 50  0001 C CNN
	1    6400 3950
	0    1    1    0   
$EndComp
$Comp
L MIDI_Split-rescue:DIN-5_180degree-MIDI_Split-rescue-MIDI_Split-rescue J8
U 1 1 5BD0D153
P 6400 4250
F 0 "J8" H 6525 4475 50  0000 C CNN
F 1 "MIDI_Thru" H 6400 4000 50  0000 C CNN
F 2 "MIDI_DIN:DIN_5_180_Degrees" H 6400 4250 50  0001 C CNN
F 3 "" H 6400 4250 50  0001 C CNN
	1    6400 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 601BA023
P 6400 3300
F 0 "#PWR011" H 6400 3050 50  0001 C CNN
F 1 "GND" H 6405 3127 50  0000 C CNN
F 2 "" H 6400 3300 50  0001 C CNN
F 3 "" H 6400 3300 50  0001 C CNN
	1    6400 3300
	-1   0    0    1   
$EndComp
$Comp
L MIDI_Split-rescue:DIN-5_180degree-MIDI_Split-rescue-MIDI_Split-rescue J7
U 1 1 5BD0D1C2
P 6400 3600
F 0 "J7" H 6525 3825 50  0000 C CNN
F 1 "MIDI_Thru" H 6400 3350 50  0000 C CNN
F 2 "MIDI_DIN:DIN_5_180_Degrees" H 6400 3600 50  0001 C CNN
F 3 "" H 6400 3600 50  0001 C CNN
	1    6400 3600
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:DIN-5_180degree-MIDI_Split-rescue-MIDI_Split-rescue J10
U 1 1 5BD0D06F
P 6400 5550
F 0 "J10" H 6525 5775 50  0000 C CNN
F 1 "MIDI_Thru" H 6400 5300 50  0000 C CNN
F 2 "MIDI_DIN:DIN_5_180_Degrees" H 6400 5550 50  0001 C CNN
F 3 "" H 6400 5550 50  0001 C CNN
	1    6400 5550
	1    0    0    -1  
$EndComp
$Comp
L MIDI_Split-rescue:DIN-5_180degree-MIDI_Split-rescue-MIDI_Split-rescue J9
U 1 1 5BD0D0EB
P 6400 4900
F 0 "J9" H 6525 5125 50  0000 C CNN
F 1 "MIDI_Thru" H 6400 4650 50  0000 C CNN
F 2 "MIDI_DIN:DIN_5_180_Degrees" H 6400 4900 50  0001 C CNN
F 3 "" H 6400 4900 50  0001 C CNN
	1    6400 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J6
U 1 1 601F24D3
P 6300 7200
F 0 "J6" H 6380 7192 50  0000 L CNN
F 1 "Conn_01x04" H 6380 7101 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6300 7200 50  0001 C CNN
F 3 "~" H 6300 7200 50  0001 C CNN
	1    6300 7200
	1    0    0    -1  
$EndComp
NoConn ~ 6700 5550
NoConn ~ 6700 4900
NoConn ~ 6700 4250
NoConn ~ 6700 3600
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R14
U 1 1 5BD0CD78
P 6850 6100
F 0 "R14" V 6930 6100 50  0000 C CNN
F 1 "220R" V 6850 6100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 6780 6100 50  0001 C CNN
F 3 "" H 6850 6100 50  0001 C CNN
	1    6850 6100
	0    1    1    0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R11
U 1 1 6050C9D6
P 6850 4150
F 0 "R11" V 6930 4150 50  0000 C CNN
F 1 "220R" V 6850 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 6780 4150 50  0001 C CNN
F 3 "" H 6850 4150 50  0001 C CNN
	1    6850 4150
	0    1    1    0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R12
U 1 1 6050A8D9
P 6850 4800
F 0 "R12" V 6930 4800 50  0000 C CNN
F 1 "220R" V 6850 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 6780 4800 50  0001 C CNN
F 3 "" H 6850 4800 50  0001 C CNN
	1    6850 4800
	0    1    1    0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R13
U 1 1 5BD10BE4
P 6850 5450
F 0 "R13" V 6930 5450 50  0000 C CNN
F 1 "220R" V 6850 5450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 6780 5450 50  0001 C CNN
F 3 "" H 6850 5450 50  0001 C CNN
	1    6850 5450
	0    1    1    0   
$EndComp
$Comp
L MIDI_Split-rescue:R-MIDI_Split-rescue-MIDI_Split-rescue R10
U 1 1 5BD0CBC5
P 6850 3500
F 0 "R10" V 6930 3500 50  0000 C CNN
F 1 "220R" V 6850 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 6780 3500 50  0001 C CNN
F 3 "" H 6850 3500 50  0001 C CNN
	1    6850 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	1300 1050 1300 950 
Wire Wire Line
	1300 950  1100 950 
Wire Wire Line
	1300 1650 1300 1700
Wire Wire Line
	6100 7050 6000 7050
Wire Wire Line
	6000 7050 6000 7200
Wire Wire Line
	6000 7200 6100 7200
Wire Wire Line
	6100 6100 6100 7050
Wire Wire Line
	6700 6100 6700 7100
Wire Wire Line
	6700 7100 6100 7100
$EndSCHEMATC
